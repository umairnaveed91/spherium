import React from 'react'
import Header from 'shared/components/common/header'
import './styles.css'

function Home() {
  return (
    <>
    <div>
      <nav>
        <Header/>
      </nav>
      <section className='FirstSec'>
        <div className='text-white text-center max-w-3xl p-4'>
          <h1 className='text-6xl'>Diversifying 
the DeFi Ecosystem </h1>
<small className='text-slate-400'>By introducing of cross-chain interoperability in everything Hyper for Defi 2.0  Defragmented</small>
        </div>
      </section>
    </div>
    </>
  )
}

export default Home
